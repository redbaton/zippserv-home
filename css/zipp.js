$(document).ready(function () {
    // alert()


    initialize_owl($('#owl1'));

    $('a[href="#home"]').on('shown.bs.tab', function () {
        initialize_owl($('#owl1'));
    }).on('hide.bs.tab', function () {
        destroy_owl($('#owl1'));
    });

    $('a[href="#profile"]').on('shown.bs.tab', function () {
        initialize_owl($('#owl2'));
    }).on('hide.bs.tab', function () {
        destroy_owl($('#owl2'));
    });

    $('a[href="#messages"]').on('shown.bs.tab', function () {
        initialize_owl($('#owl3'));
    }).on('hide.bs.tab', function () {
        destroy_owl($('#owl3'));
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href") // activated tab
        alert(target);
    });


});


$(window).load(function () {

    var viewportWidth = $(window).width();
    if (viewportWidth < 600) {
        $("#HomeCarousel").addClass("gallery-mobile");
    }

    $(window).resize(function () {

        if (viewportWidth < 600) {
            $("#HomeCarousel").addClass("gallery-mobile");
        }
    });
});

function initialize_owl(el) {
    el.owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 1,
                nav: false
            },
            1000: {
                items: 1,
                nav: true,
                loop: false
            }
        }
    });
}

function destroy_owl(el) {
    el.data('owlCarousel').destroy();
}